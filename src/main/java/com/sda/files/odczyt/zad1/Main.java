package com.sda.files.odczyt.zad1;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        File plik = new File("plik.txt");
        if(plik.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(plik))) {
                String linia = reader.readLine();

                if (linia.equals("Hello World!")) {
                    System.out.println("Poprawna linia");
                } else {
                    System.out.println("Plik zawiera coś innego!");
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            System.out.println("Plik nie istnieje!");
        }
    }
}

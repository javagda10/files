package com.sda.files.zapis.zad6;

import com.sda.files.zapis.zad5.Formularz;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        boolean czyPracuje = true;
        while (czyPracuje) {
            Formularz formularz = new Formularz();

            System.out.println("Wiek?");
            formularz.setWiek(scanner.nextInt());

            System.out.println("Wzrost?");
            formularz.setWzrost(scanner.nextInt());

            System.out.println("Czy jesteś kobietą?");
            formularz.setCzyKobieta(scanner.nextBoolean());

            System.out.println("Zarobki?");
            formularz.setZarobki(scanner.nextInt());

            System.out.println("Czy masz psa?");
            formularz.setCzyMaPsa(scanner.nextBoolean());

            System.out.println("Numer buta?");
            formularz.setRozmiarButa(scanner.nextInt());

            try (PrintWriter writer = new PrintWriter(new FileWriter("formularze.txt", true))) {
                writer.println(formularz.zwrocWFormacie());
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Czy chcesz kontynuować??");
            if(!scanner.nextBoolean()){
                czyPracuje = false;
            }
        }
    }
}

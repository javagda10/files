package com.sda.files.zapis.zad5;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Formularz formularz = new Formularz();

        System.out.println("Wiek?");
        formularz.setWiek(scanner.nextInt());

        System.out.println("Wzrost?");
        formularz.setWzrost(scanner.nextInt());

        System.out.println("Czy jesteś kobietą?");
        formularz.setCzyKobieta(scanner.nextBoolean());

        System.out.println("Zarobki?");
        formularz.setZarobki(scanner.nextInt());

        System.out.println("Czy masz psa?");
        formularz.setCzyMaPsa(scanner.nextBoolean());

        System.out.println("Numer buta?");
        formularz.setRozmiarButa(scanner.nextInt());

        try(PrintWriter writer = new PrintWriter("formularze.txt")){
            writer.println(formularz.zwrocWFormacie());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

package com.sda.files.zapis;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;

//        File plik = new File("/home/amen/Projects/files");
        File plik = new File("../");
        System.out.println(plik.getAbsolutePath());


        try (PrintWriter printWriter = new PrintWriter(new FileWriter("plik.txt", true))) {
            while (isWorking) {
                String linia = scanner.nextLine();

                if (linia.equals("quit")) {
                    break;
                }

                printWriter.println(linia);
                printWriter.flush();

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.sda.files;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        File plik = new File("plik.txt");
        try {
            Scanner scanner = new Scanner(plik);
            while (scanner.hasNextLine()) {
                String linia = scanner.nextLine();
                System.out.println(linia);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(plik))) {
//            String linia = reader.readLine();
//            while (linia != null) {
//                System.out.println(linia);
//                linia = reader.readLine();
//            }

            String linia;
            while ((linia = reader.readLine()) != null) {
                System.out.println(linia);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
